require('dotenv').config();

const { Client, WebhookClient } = require('discord.js');

const client = new Client({
    partials: ['MESSAGE', 'REACTION']
});

const webhookClient = new WebhookClient(
    process.env.WEBHOOK_ID,
    process.env.WEBHOOK_TOKEN)

const PREFIX = "$";

client.on('ready', () => {
    console.log(`${client.user.tag} has logged in`);
});

client.on('message', (message) => {
    if (message.author.bot) return;
    console.log(message.content);

    if (message.guild) {
        if (message.content.startsWith(PREFIX)) {
            const [CMD_NAME, ...args] = message.content
                .trim()
                .substring(PREFIX.length)
                .split(/\s+/);

            if (CMD_NAME === 'kick') {
                if (!message.member.hasPermission('KICK_MEMBERS')) {
                    return message.reply("You do not have permissions to use the kick command")
                }
                if (args.length === 0) return message.reply('please provide an ID');

                const member = message.guild.members.cache.get(args[0]);

                if (member) {
                    member.kick()
                        .then(() => message.channel.send(`${member} was kicked`))
                        .catch((error) => message.channel.send(`I cannot kick the user`));
                } else {
                    message.channel.send('That member is not found');
                }
            }
            else if (CMD_NAME == 'ban') {
                if (!message.member.hasPermission('BAN_MEMBERS')) {
                    return message.reply("You do not have permissions to use the ban command");
                }
                if (args.length === 0) return message.reply('please provide an ID');

                const member = message.guild.members.cache.get(args[0]);

                if (member) {
                    member.ban()
                        .then(() => message.channel.send(`${member} was Banned`))
                        .catch((error) => message.channel.send(`I cannot ban the user`));
                } else {
                    message.channel.send('That member is not found');
                }
            }
            else if(CMD_NAME === 'announce'){
                console.log(args);
                const msg = args.join(' ');
                console.log(msg);
                webhookClient.send(msg);
            }
        }
    } else {
        console.log('Message received in a direct message');
    }
});

client.on('messageReactionAdd', (reaction, user) =>{
    const {name} = reaction.emoji;
    const member = reaction.message.guild.members.cache.get(user.id);

    if(reaction.message.id === '1147537739311493202'){
        switch(name){
            case '🍎':
                member.roles.add('1147541677725208657');
                break;
                
            case '🍌':
                member.roles.add('1147541746344013856');
                break;
            
            case '🍇':
                member.roles.add('1147541800844800011');
                break;
            
            case '🍑':
                member.roles.add('1147541949918744586');
                break;
            
        }

    }
});

client.on('messageReactionRemove', (reaction, user) =>{
    const {name} = reaction.emoji;
    const member = reaction.message.guild.members.cache.get(user.id);

    if(reaction.message.id === '1147537739311493202'){
        switch(name){
            case '🍎':
                member.roles.remove('1147541677725208657');
                break;
                
            case '🍌':
                member.roles.remove('1147541746344013856');
                break;
            
            case '🍇':
                member.roles.remove('1147541800844800011');
                break;
            
            case '🍑':
                member.roles.remove('1147541949918744586');
                break;
            
        }

    }
});

client.login(process.env.DISCORDJS_BOT_TOKEN);
